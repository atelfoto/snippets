<h1 align="center">
S n i p p e t s

![logo](images/cakephp.png)

</h1>

## Snippets Cakephp for VS Code

You can install this plugin into your CakePHP application using [Bitbucket.org](https://bitbucket.org/atelfoto/snippets/src/master/).


| Snippet            | Prefix        | Description                       |                                             |
| ------------------ | ------------- | --------------------------------- | ------------------------------------------- |
| `success`          | success       | $this->Flash->success();          |
| `error `           | error         | $this->Flash->error();            |
| `url`              | url           | $this->Html->url()                |
| `css`              | css           | $this->Html->css()                |
| `script`           | script        | $this->Html->script()             |
| `link`             | link          | $this->Html->link()               |
| `ìmg`              | img           | $this->Html->image()              |
| `ìmgLink`          | imgL          | $this->Html->image(path,opt,url)  |
| `ele`              | ele           | $this->Element()                  |
| `layout`           | setLayout     | $this->viewBuilder()->setLayout() |
| `start`            | start         | $this->start()                    |
| `append`           | append        | $this->append()                   |
| `prepend`          | prepend       | $this->prepend()                  |
| `assign`           | assigne       | $this->assigne()                  |
| `reset`            | reset         | $this->reset()                    |
| `fetch`            | fetch         | $this->fetch()                    |
| `truncate`         | truncate      | $this->Text->truncate(text,22,[]) |
| `highlight`        | highlight     | $this->Text->highlight()          |
| `autoParagraph`    | autoParagraph | $this->Text->autoParagraph()      |
| `data`             | data          | $this->request->getData()         |
| `param`            | param         | $this->request->Param()           |
| `find`             | find          | $this->Models->find()             |
| `save`             | save          | $this->Models->save()             |
| `loadModel`        | tlm           | $this->loadModel()                |
| `loggedIn`         | loggedIn      | $this->Auth->loggedIn()           |
| `loggin`           | loggin        | $this->Auth->loggin()             |
| `logout`           | logout        | $this->Auth->logout()             |
| `mapActions`       | mapActions    | $this->Auth->mapActions()         |
| `redirectUrl`      | redirectUrl   | $this->Auth->redirectUrl()        |
| `allow`            | allow         | $this->Auth->allow()              |
| `authenticate`     | authenticate  | $this->Auth->authenticate()       |
| `deny`             | deny          | $this->Auth->deny()               |
| `redirect`         | redirect      | return $this->redirect()          |
| `set`              | set           | $this->set()                      |
| `compact`          | compact       | $this->set(compact())             |
| `hasMany`          | thm           | $this->hasMany()                  |
| `die`              | die           | die()                             |
| `debug`            | db            | debug()                           |
| `debug`            | dbta          | debug($query->toArray());         | Affiche les résultats sous forme de tableau |
| `die(json_encode)` | dj            | die(json_encode())                |
| `Configure`        | configure     | Configure->read()                 |
| `conditions`       | conditions    | ->conditions([])                  |
| `create`           | create        | $this->Form->create();            |
| `end`              | end           | $this->Form->end()                |
| `control`          | control       | $this->Form->control()            |
| `checkbox`         | checkbox      | $this->Form->checkbox()           |
| `label`            | label         | $this->Form->label()              |
| `text`             | text          | $this->Form->text()               |
| `password`         | password      | $this->Form->password()           |
| `hidden`           | hidden        | $this->Form->hidden()             |
| `textarea`         | textarea      | $this->Form->textarea()           |
| `radio`            | radio         | $this->Form->radio()              |
| `select`           | select        | $this->Form->select()             |
| `file`             | file          | $this->Form->file()               |
| `button`           | button        | $this->Form->button()             |
| `postButton`       | postButton    | $this->Form->postButton()         |
| `postLink`         | postLink      | $this->Form->postLink()           |
| `year`             | year          | $this->Form->year()               |
| `month`            | month         | $this->Form->month()              |
| `day`              | day           | $this->Form->day()                |
| `hour`             | hour          | $this->Form->hour()               |
| `meridian`         | meridian      | $this->Form->meridian()           |
| `error`            | error         | $this->Form->error()              |
| `isFieldError`     | isFieldError  | $this->Form->isFieldError()       |
| `unlockField`      | unlockField   | $this->Form->unlockField()        |
| `secure`           | secure        | $this->Form->secure()             |
| `submit`           | submit        | $this->Form->submit()             |



