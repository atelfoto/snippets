Composer
========
Créée le jeudi 06 mai 2021

Mise à jour manuel de composer
------------------------------


1. Mise à jour dans la Liberkey
	* Se référer  au pages suivantes  [forum de laragon](https://forum.laragon.org/topic/2222/upgrade-to-composer-2-x/4) et [la mise à jour](https://getcomposer.org/download/) .
	* Vous devez d'abord changer votre répertoire en (Votre nom de disque):\LiberKey\MyApps\laragon\bin\composer ( ou L : \  pour un disque dur externe ).

	cd L:\Liberkey\MyApps\laragon\bin\composer



* Executer composer -V pour obtenir la version actuelle
```bash
 composer -V
```
**Remarque**: Dans visual Studio Code quand Cmder est ouvert le terminal interne ne s'ouvre pas sauf en nouveau terminal (ctrl+maj+ù).

```bash
L:\Liberkey\MyApps\laragon\bin\composer
$ composer -V
```



![](images/composer-01.jpg)


## Installation en ligne de commande  ##

Suivre les étapes de getComposer.org/download/


* Téléchargez le programme d'installation dans le répertoire actuel
* Vérifiez le programme d'installation SHA-384
* Exécutez le programme d'installation
* Supprimer le programme d'installation


![](images/composer-02.jpg)



* Supprimer le programme d'installation
* Vérifier la nouvelle version


![](images/composer-03.jpg)



Seconde solution
----------------

Dans un terminal insérer

	composer self-update

 et composer ```self-update``` --rollback pour revenir à l'ancienne version.

![](images/composer-04.jpg)


test
----



