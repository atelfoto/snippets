 # Snippets for Visual Studio code  <!-- no toc -->

**Table des matières**

  - [1. scss](#1-scss)
  - [2. cakephp](#2-cakephp)
  - [3. zim](#3-zim)

## 1. scss
* [Mixins breakpoint ](scss.md)

## 2. cakephp
* [snippets for cakephp](cakephp.md)
## 3. zim
* [snippets for zim](zim.md)