# S n i p p e t s for scss

You can install this snippets from [Bitbucket.org](https://bitbucket.org/atelfoto/workspace/snippets/9Xdgr5).

## Media Queries

| body                                    | Prefix | Description          | --- |
| --------------------------------------- | ------ | -------------------- | --- |
| `@include respond-above(xs) {$1}`       | raxs   | au-dessus de 576px   |
| `@include respond-above(sm) {$1}`       | rasm   | au-dessus de 768px   |
| `@include respond-above(md) {$1}`       | ramd   | au-dessus de 992px   |
| `@include respond-above(lg) {$1}`       | ralg   | au-dessus de 1200px  |
| `@include respond-above(xl) {$1}`       | raxl   | au-dessus de 1500px  |
| `@include respond-below(xs) {$1}`       | rbxs   | en-dessous de 576px  |
| `@include respond-below(sm) {$1}`       | rbsm   | en-dessous de 768px  |
| `@include respond-below(md) {$1}`       | rbmd   | en-dessous de 992px  |
| `@include respond-below(lg) {$1}`       | rblg   | en-dessous de 1200px |
| `@include respond-below(xl) {$1}`       | rbxl   | en-dessous de 1500px |
| `@include respond-between($1, $2) {$3}` | rbtw   | compris entre x et y |

## For Html5

| body                                         | Prefix | Description                                   |
| -------------------------------------------- | ------ | --------------------------------------------- |
| `@include breakpoints((`                     |        |                                               |
| ` xlarge: ( 1281px, 1680px, )`               |        |                                               |
| ` large: ( 981px, 1280px, ),`                |        |                                               |
| ` medium: ( 737px, 980px, ),`                | br     | breakpoint                                    |
| ` small: ( 481px, 736px, ),`                 |        |                                               |
| ` xsmall: ( 361px, 480px, ),`                |        |                                               |
| ` xxsmall: ( null, 360px, ),`                |        |                                               |
| `));`                                        |        |                                               |
| _____________________________________________________________________________________________________ |
| `@include breakpoint('${1:<=}xlarge') {$2}`  | bxl    | extra large                                   |
| `@include breakpoint('${1:<=}large') {$2}`   | bl     | large                                         |
| `@include breakpoint('${1:<=}medium') {$2}`  | bm     | medium                                        |
| `@include breakpoint('${1:<=}small') {$2}`   | bs     | small <= 736 or >= 481 or small (481 and 736) |
| `@include breakpoint('${1:<=}xsmall') {$2}`  | bxs    | extra small                                   |
| `@include breakpoint('${1:<=}xxsmall') {$2}` | bxxs   | big extra small                               |

### Exemples

![gif](images/2021-05-25_19-42-24.gif)

* Breakpoint to declare in a SCSS file

```scss
@include breakpoints(
  (
    xlarge: (
      1281px,
      1680px,
    ),
    large: (
      981px,
      1280px,
    ),
    medium: (
      737px,
      980px,
    ),
    small: (
      481px,
      736px,
    ),
    xsmall: (
      361px,
      480px,
    ),
    xxsmall: (
      null,
      360px,
    ),
  )
);
```

* Prefix to enter a SCSS file
```scss
 bxl
 bl
 bm
 bs
 bxs
 bxxs
```
* Result in a CSS file

```css
/* bxl <=xlarge */
@media screen and (max-width: 1680px) {
  .toto {
    width: 100px;
  }
}
/*bxl >=xlarge */
@media screen and (min-width: 1281px) {
  .toto {
    width: 100px;
  }
}
/*bxl <=xlarge */
@media screen and (min-width: 1281px) and (max-width: 1680px) {
  .toto {
    width: 100px;
  }
}
/*bl  <=large */
@media screen and (max-width: 1280px) {
  .toto {
    width: 100px;
  }
}
/*bl >=large */
@media screen and (min-width: 981px) {
  .toto {
    width: 100px;
  }
}
/*bl  large */
@media screen and (min-width: 981px) and (max-width: 1280px) {
  .toto {
    width: 100px;
  }
}
/*bm <=medium */
@media screen and (max-width: 980px) {
  .toto {
    width: 100px;
  }
}
/*bm >=medium */
@media screen and (min-width: 737px) {
  .toto {
    width: 100px;
  }
}
/*bm medium */
@media screen and (min-width: 737px) and (max-width: 980px) {
  .toto {
    width: 100px;
  }
}
/*bs <=small */
@media screen and (max-width: 736px) {
  .test {
    width: 1%;
  }
}
/*bs >=small */
@media screen and (min-width: 481px) {
  .test {
    width: 1%;
  }
}
/*bs small */
@media screen and (min-width: 481px) and (max-width: 736px) {
  .test {
    width: 1%;
  }
}
/*bxs <=xsmall */
@media screen and (max-width: 480px) {
  .toto {
    width: 100px;
  }
}
/*bxs >=xsmall */
@media screen and (min-width: 361px) {
  .toto {
    width: 100px;
  }
}
/*bxs xsmall */
@media screen and (min-width: 361px) and (max-width: 480px) {
  .toto {
    width: 100px;
  }
}
/*bxxs <=xxsmall */
@media screen and (max-width: 360px) {
  .toto {
    width: 100px;
  }
}
/*bxxs >=xxsmall */
@media screen {
  .toto {
    width: 100px;
  }
}
/*bxss xxsmall */
@media screen and (max-width: 360px) {
  .toto {
    width: 100px;
  }
```
## Font-face
*Obligatoire
- **$name:** Nom de la police *
- **$path:** Chemin vers les fichiers *
- **$weight:** Graisse par défaut sur null
- **$style:** Le font-style, par défaut sur null
- **$exts:** Extension des fichiers à inclure par défaut la totalité
  ```scss
  @include font-face($name, $path, $weight: null, $style: null, $exts: eot woff2 woff ttf svg);
  ```

### Exemple

1. Créez une règle de police de caractères. Les fichiers OpenType, WOFF2, WOFF, TrueType et SVG sont intégrés automatiquement.
   ```scss
   @include font-face(Samplino, fonts/Samplino);
   ```
   Rendu en css
   ```css
   @font-face {
   	  font-family: "Samplino";
   	  src: url("fonts/Samplino.eot?") format("eot"),
   		     url("fonts/Samplino.woff2") format("woff2"),
   		     url("fonts/Samplino.woff") format("woff"),
   		     url("fonts/Samplino.ttf") format("truetype"),
   		     url("fonts/Samplino.svg#Samplino") format("svg");
   }
   ```
2. Créez une règle de police de caractères qui s'applique au texte en gras et en italique avec la totalité des format.
   ```scss
   @include font-face("Samplina Neue", fonts/SamplinaNeue, bold, italic);
   ```
   rendu en css
   ```css
   @font-face {
	  font-family: "Samplina Neue";
	  src: url("fonts/SamplinaNeue.eot?") format("eot"),
	       url("fonts/SamplinaNeue.woff2") format("woff2"),
	       url("fonts/SamplinaNeue.woff") format("woff"),
	       url("fonts/SamplinaNeue.ttf") format("truetype"),
	       url("fonts/SamplinaNeue.svg#Samplina_Neue") format ("svg");
	  font-weight: bold;
	  font-style: italic;
   }
   ```

3. Créez une règle de police de caractères qui s'applique au texte et aux sources de poids 500 avec un style normal et des extensions EOT, WOFF2 et WOFF.
   ```scss
   @include font-face(Samplinal, fonts/Samplinal, 500, normal, eot woff2 woff);
   ```
   rendu en css

   ```css
   @font-face {
  	font-family: "Samplinal";
  		src: url("fonts/Samplinal.eot?") format("eot"),
  	     url("fonts/Samplinal.woff2") format("woff2"),
  	     url("fonts/Samplinal.woff") format("woff");
  	font-weight: 500;
  	font-style: normal;
   }
   ```
4. Créez une règle de police de caractères qui ne génère qu'un WOFF.
   ```scss
   @include font-face(Samplinoff, fonts/Samplinoff, null, null, woff);
   ```
   Rendu en css
   ```css
   @font-face {
  	font-family: "Samplinoff";
  	src: url("fonts/Samplinoff.woff") format("woff");
   }
   ```